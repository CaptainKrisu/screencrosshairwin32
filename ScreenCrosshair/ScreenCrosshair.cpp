// ScreenCrosshair.cpp : Defines the entry point for the application.
//

#include "framework.h"
#include "ScreenCrosshair.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
MONITORINFO mi;									// primary monitor info
HWND winhWnd;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);

BOOL GetMessageWithTimeout(MSG* msg, UINT to)
{
	BOOL res;
	UINT_PTR timerId = SetTimer(NULL, NULL, to, NULL);
	res = GetMessage(msg, NULL, 0, 0);
	KillTimer(NULL, timerId);
	if (!res)
		return FALSE;
	if (msg->message == WM_TIMER && msg->hwnd == NULL && msg->wParam == timerId)
		return FALSE; //TIMEOUT! You could call SetLastError() or something...
	return TRUE;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
					  _In_opt_ HINSTANCE hPrevInstance,
					  _In_ LPWSTR    lpCmdLine,
					  _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_SCREENCROSSHAIR, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow)) {
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SCREENCROSSHAIR));

	MSG msg;

	// Main message loop:
	while (true) {
		if (GetMessageWithTimeout(&msg, NULL)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		InvalidateRect(winhWnd, nullptr, TRUE);
		UpdateWindow(winhWnd);
		RedrawWindow(winhWnd, NULL, NULL, RDW_UPDATENOW);
	}

	return (int)msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SCREENCROSSHAIR));
	wcex.hCursor = NULL;
	wcex.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_SCREENCROSSHAIR);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	winhWnd = CreateWindowExW(NULL, szWindowClass, szTitle, WS_POPUP,
							  0, 0, CW_USEDEFAULT, CW_USEDEFAULT, nullptr, nullptr, hInstance, nullptr);

	if (!winhWnd) {
		return FALSE;
	}

	HMONITOR hmon = MonitorFromWindow(winhWnd,
									  MONITOR_DEFAULTTOPRIMARY);
	mi = { sizeof(mi) };
	if (!GetMonitorInfo(hmon, &mi)) return NULL;

	SetWindowPos(winhWnd, HWND_TOPMOST, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, NULL);

	BOOL enabled;
	if (SUCCEEDED(DwmIsCompositionEnabled(&enabled)) && enabled) {
		DWM_BLURBEHIND bb = { 0 };
		bb.dwFlags = DWM_BB_ENABLE | DWM_BB_BLURREGION;
		bb.hRgnBlur = CreateRectRgn(0, 0, -1, -1);
		bb.fTransitionOnMaximized = 1;
		bb.fEnable = TRUE;

		if (SUCCEEDED(DwmEnableBlurBehindWindow(winhWnd, &bb))) {
			MARGINS m = { -1 };
			DwmExtendFrameIntoClientArea(winhWnd, &m);

			LONG exStyle = GetWindowLongW(winhWnd, GWL_EXSTYLE);

			exStyle |= WS_EX_LAYERED | WS_EX_TRANSPARENT;
			//exStyle |= WS_EX_TOOLWINDOW; // Hide from taskbar

			SetWindowLongW(winhWnd, GWL_EXSTYLE, exStyle);
			SetLayeredWindowAttributes(winhWnd, 0, 255, LWA_ALPHA);
		}
	} else {
		LONG exStyle = GetWindowLongW(winhWnd, GWL_EXSTYLE);
		exStyle &= ~WS_EX_LAYERED;
		SetWindowLongW(winhWnd, GWL_EXSTYLE, exStyle);
		RedrawWindow(winhWnd, NULL, NULL, RDW_ERASE | RDW_INVALIDATE | RDW_FRAME);
	}

	ShowWindow(winhWnd, nCmdShow);
	UpdateWindow(winhWnd);

	SetCapture(winhWnd);
	ShowCursor(FALSE);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
		case WM_NCHITTEST:
			return HTTRANSPARENT;
		case WM_SETCURSOR:
		{
			if (LOWORD(lParam) == HTCLIENT) {
				SetCursor(NULL);
				return TRUE;
			}
		}
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);

			HDC memDC = CreateCompatibleDC(hdc);

			// now we can create bitmap where we shall do our drawing
			HBITMAP bmp = CreateCompatibleBitmap(hdc,
												 mi.rcMonitor.right - mi.rcMonitor.left,
												 mi.rcMonitor.bottom - mi.rcMonitor.top);

			// we need to save original bitmap, and select it back when we are done,
			// in order to avoid GDI leaks!
			HBITMAP oldBmp = (HBITMAP)SelectObject(memDC, bmp);

			// Drawing
			POINT cursorPos;
			if (GetCursorPos(&cursorPos))
				ScreenToClient(hWnd, &cursorPos);

			RECT rc;
			rc.left = mi.rcMonitor.left;
			rc.top = mi.rcMonitor.top;
			rc.right = mi.rcMonitor.right;
			rc.bottom = mi.rcMonitor.bottom;
			auto bruh = CreateSolidBrush(RGB(0, 0, 0));
			FillRect(memDC, &rc, bruh);

			auto pen = CreatePen(PS_SOLID, 3, RGB(255, 0, 255));
			auto oldPen = SelectObject(memDC, pen);
			MoveToEx(memDC, cursorPos.x, 0, nullptr);
			LineTo(memDC, cursorPos.x, mi.rcMonitor.bottom);
			MoveToEx(memDC, 0, cursorPos.y, nullptr);
			LineTo(memDC, mi.rcMonitor.right, cursorPos.y);
			SelectObject(memDC, oldPen);
			DeleteObject(pen);
			// EOF drawing

			BitBlt(hdc, 0, 0, mi.rcMonitor.right - mi.rcMonitor.left,
				   mi.rcMonitor.bottom - mi.rcMonitor.top, memDC, 0, 0, SRCCOPY);

			// all done, now we need to cleanup
			SelectObject(memDC, oldBmp); // select back original bitmap
			DeleteObject(bmp); // delete bitmap since it is no longer required
			DeleteDC(memDC);   // delete memory DC since it is no longer required

			EndPaint(hWnd, &ps);
		}
		break;
		case WM_ERASEBKGND: // reduces flicker while we don't have doublebuffering done
			return TRUE;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}